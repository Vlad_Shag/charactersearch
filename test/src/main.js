import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import store from './store'
import BootstrapVue  from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios';
import Routes from './routes';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false

const router = new VueRouter({
    routes: Routes
}); 

new Vue({
  render: h => h(App),
  router: router,
  store,
}).$mount('#app')
