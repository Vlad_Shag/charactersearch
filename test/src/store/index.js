import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        history: [],
    },

    mutations: {
        setElInHistory(state, el) {
            let isAdded = false;
            for(let i = 0; i < state.history.length; i++) {
                if(state.history[i].name == el.name) {
                    isAdded = true;
                }
            }
            if(!isAdded) {
                state.history.push(el);
            }
        }
    }
})