import detail from './components/detail.vue';
import search from './components/search.vue';

const routes = [
    { path: '/', component: search, name: 'search' },
    { path: '/detail/:url', component: detail, name: 'detail' },
];
export default routes;